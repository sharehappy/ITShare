面试题

### java基础

1. 常见的异常有哪些？java怎样知道该抛对应的异常，比如NullPointerException?
NullpointerException
FileNotFoundException
InterruptedException
IllegalArgsException
ClassNotFoundException
ClassCastException
IOException

2. wait() sleep()的区别，在哪个类中？
	wait() java.lang.Object
	sleep()  java.lang.Thread
	wait需要通过notify或者notifyAll()唤醒
	sleep可以设置睡眠时间，睡眠时间过后会继续执行后面的程序
	
3. IO/NIO的区别，为什么要用NIO，使用IO中的Buffered..也能实现NIO的面向缓冲，什么情况下用NIO？

4. 你熟悉的排序算法有哪些，快速排序算法是怎么实现的，实现原理怎样的？

参考：
https://mp.weixin.qq.com/s/E-adFoMHYAJ8-lnXjzbZZg
https://www.cs.usfca.edu/~galles/visualization/Algorithms.html

5. java8的新特性
	a.lambda表达式
	b.时间类型增加个LocalDateTime LocalDate  Instance
	c.接口增加了默认方法
	d.函数式接口
	e.concurrentHashMap底层数据接口改成红黑树，解决了死锁
	f.多重注解
	g.lambda表达式内访问对象字段和静态变量
	h.lambda表达式内可以访问局部变量
	i.方法与构造函数引用 通过 :: 可以获取构造函数的引用
		
	参考：https://www.jianshu.com/p/0bf8fe0f153b
	
6. 注解来自于哪里，怎么使用？
	注解源于 jdk，如果要使用注解可以使用反射找到包含该注解的类和方法，然后对用了注解的类和方法处理即可
	
7. 动态代理实现原理是什么和动态代理使用的方法、类有哪些？

8. webservice hessian区别？

9. 网页中常出现的代码？
 200 ok 403 访问被禁止 404 页面找不到 500 服务器问题 
 参考：http://www.cnblogs.com/ctaixw/p/5075727.html 
  
10.==hashmap底层结构怎样的？ #F44336==（问的较多）
	数组+链表
	java8之后从第8个开始采用红黑树

11.ConcurrentHashMap怎样实现线程安全的？
 分段锁Segment 1.7
 node+sychronized+红黑树 1.8
 底层使用了ReentrantLock
 


12.CAS底层数据结构有看过吗？ ABA


13.你知道的线程安全的类有哪些，方法有哪些？ 
常用的集合都不是线程安全的，安全的需要使用concurrent StringBuilder vector线程安全

14.forward redirect的区别？
	forward:转发
	redirect:重定向
转发是服务器行为，重定向是客户端行为。

转发（Forword） 通过RequestDispatcher对象的forward（HttpServletRequest request,HttpServletResponse response）方法实现的。RequestDispatcher 可以通过HttpServletRequest 的 getRequestDispatcher()方法获得。例如下面的代码就是跳转到 login_success.jsp 页面。

request.getRequestDispatcher("login_success.jsp").forward(request, response);
重定向（Redirect） 是利用服务器返回的状态码来实现的。客户端浏览器请求服务器的时候，服务器会返回一个状态码。服务器通过HttpServletRequestResponse的setStatus(int status)方法设置状态码。如果服务器返回301或者302，则浏览器会到新的网址重新请求该资源。

从地址栏显示来说：forward是服务器请求资源，服务器直接访问目标地址的URL，把那个URL的响应内容读取过来，然后把这些内容再发给浏览器。浏览器根本不知道服务器发送的内容从哪里来的，所以它的地址栏还是原来的地址。redirect是服务端根据逻辑，发送一个状态码，告诉浏览器重新去请求那个地址。所以地址栏显示的是新的URL。
从数据共享来说：forward：转发页面和转发到的页面可以共享request里面的数据。redirect：不能共享数据。
从运用地方来说：forward：一般用于用户登陆的时候，根据角色转发到相应的模块。redirect：一般用于用户注销登陆时返回主页面和跳转到其它的网站等。
从效率来说：forward：高。redirect：低。
来源：https://gitee.com/sharehappy/JavaGuide/blob/master/docs/essential-content-for-interview/PreparingForInterview/%E7%BE%8E%E5%9B%A2%E9%9D%A2%E8%AF%95%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98%E6%80%BB%E7%BB%93.md

15.yaml是什么？
是一个可读性高，用来表达数据序列化的格式

16.熟悉哪些设计模式，写出伪代码
	单例设计模式
	工厂模式
	装饰设计模式
	代理设计模式
	适配器模式
	构建者设计模式
	参考：https://mp.weixin.qq.com/s/vazjj_UMD2WqaQ4SG-yhCA


17.手写冒泡排序算法
	
``` 
public static void bubbleSort(int[] arrays, boolean flag) {
        int tmp;
        for (int i=0;i<arrays.length;i++) {
            for (int j=i+1;j<arrays.length;j++) {
                /*
                 * 升序
                 */
                if (flag) {
                    if (arrays[i] > arrays[j]) {
                        tmp = arrays[i];
                        arrays[i] = arrays[j];
                        arrays[j] = tmp;
                    }
                } else {
                    if (arrays[i] < arrays[j]) {
                        tmp = arrays[i];
                        arrays[i] = arrays[j];
                        arrays[j] = tmp;
                    }
                }
            }
        }
    }
```

18.接口与抽象类的区别？

参考：https://segmentfault.com/a/1190000019895958

19.用过的中间件有哪些？
	redis
	rocketMQ（消息中间件） 
	apollo（配置中心)
	zookeeper:
	hystrix:熔断
	
20.3次握手的原理是什么，4次挥手的原理是什么？
简单示意图： TCP三次握手

客户端–发送带有 SYN 标志的数据包–一次握手–服务端
服务端–发送带有 SYN/ACK 标志的数据包–二次握手–客户端
客户端–发送带有带有 ACK 标志的数据包–三次握手–服务端
为什么要三次握手
三次握手的目的是建立可靠的通信信道，说到通讯，简单来说就是数据的发送与接收，而三次握手最主要的目的就是双方确认自己与对方的发送与接收是正常的。

第一次握手：Client 什么都不能确认；Server 确认了对方发送正常，自己接收正常。

第二次握手：Client 确认了：自己发送、接收正常，对方发送、接收正常；Server 确认了：自己接收正常，对方发送正常

第三次握手：Client 确认了：自己发送、接收正常，对方发送、接收正常；Server 确认了：自己发送、接收正常，对方发送、接收正常

所以三次握手就能确认双发收发功能都正常，缺一不可。

为什么要传回 SYN
接收端传回发送端所发送的 SYN 是为了告诉发送端，我接收到的信息确实就是你所发送的信号了。

SYN 是 TCP/IP 建立连接时使用的握手信号。在客户机和服务器之间建立正常的 TCP 网络连接时，客户机首先发出一个 SYN 消息，服务器使用 SYN-ACK 应答表示接收到了这个消息，最后客户机再以 ACK(Acknowledgement[汉译：确认字符 ,在数据通信传输中，接收站发给发送站的一种传输控制字符。它表示确认发来的数据已经接受无误。 ]）消息响应。这样在客户机和服务器之间才能建立起可靠的TCP连接，数据才可以在客户机和服务器之间传递。

传了 SYN,为啥还要传 ACK
双方通信无误必须是两者互相发送信息都无误。传了 SYN，证明发送方（主动关闭方）到接收方（被动关闭方）的通道没有问题，但是接收方到发送方的通道还需要 ACK 信号来进行验证。

TCP四次挥手

断开一个 TCP 连接则需要“四次挥手”：

客户端-发送一个 FIN，用来关闭客户端到服务器的数据传送
服务器-收到这个 FIN，它发回一 个 ACK，确认序号为收到的序号加1 。和 SYN 一样，一个 FIN 将占用一个序号
服务器-关闭与客户端的连接，发送一个FIN给客户端
客户端-发回 ACK 报文确认，并将确认序号设置为收到序号加1
为什么要四次挥手
任何一方都可以在数据传送结束后发出连接释放的通知，待对方确认后进入半关闭状态。当另一方也没有数据再发送的时候，则发出连接释放通知，对方确认后就完全关闭了TCP连接。

举个例子：A 和 B 打电话，通话即将结束后，A 说“我没啥要说的了”，B回答“我知道了”，但是 B 可能还会有要说的话，A 不能要求 B 跟着自己的节奏结束通话，于是 B 可能又巴拉巴拉说了一通，最后 B 说“我说完了”，A 回答“知道了”，这样通话才算结束。

上面讲的比较概括，推荐一篇讲的比较细致的文章：https://blog.csdn.net/qzcsu/article/details/72861891

21.Integer与int的区别，什么情况下使用Integer什么情况下使用int，Integer是否有缓存？
int基本数据类型，Integer为int的包装类型，
Integer 缓存了-128~127 在此范围内用==比较两个数字时返回true超出范围返回false

22.拦截器与过滤器的区别？
拦截器基于反射机制主要作用于类

过滤器可以拦截任何请求，可以作用域方法之上，但是依赖于servlet容器


23.webservice使用的的什么语言，使用的什么协议？
wsdl描述语言 soap协议
String a = "abc";  String b = new String("abc");  两个对象a b相等吗，区别在哪？

24.字符串abcd怎样获取abcd、acbd、acdb、adbc、adcb、bacd、bcad、bdac、bdca、cabd、cdba、cadb、cbda等

25.异常

26.输入输出流
ObjectInputStream 
ObjectOutputStream

27.cookie、session、token的区别

参考：https://www.cnblogs.com/moyand/p/9047978.html

28.引用数据类型与基本数据类型区别？
基本数据类型是分配在栈上的，而引用类型是分配在堆上的

参考：https://www.cnblogs.com/moyand/p/9047978.html
https://www.cnblogs.com/xiaobaizhang/p/7783106.html
https://www.cnblogs.com/cuteboy/p/8646526.html

# 线程
1.创建线程的几种方式？
	继承Thread
	实现Runnable接口
	
2.进程之间通过什么方式通信？
共享内存、管道、消息队列、socket、文件、信号量

3.进程是什么，线程是什么？

4.线程实现的方式有几种，一般用哪个，为什么要用那个？
集成Thread 实现Runable接口 java单继承一般用接口

6.多线程中线程池怎样使用及其实现原理？

7.vilotile关键字的作用是什么？ 
 让线程可见，怎样实现可见和不可见
 vilotile标识都会刷新到内存

8.synchronized关键字的作用，使用该关键字后保证同步了，同步代码块与同步方法有什么区别？

9.多个对象对同步方法一定能保证同步吗？多个对象能对同步方法和静态同步方法保证同步吗？两个静态同步方法能保证同步吗？

10.start方法与run方法的区别？
	start方法能够启动一个线程
	run方法只是一个普通的方法不会单独启动一个线程

11.一个线程可以多次start吗，会报错吗？ 
这个会报非法异常

参考：http://www.sohu.com/a/321408502_663371

12.线程中通过什么方式传输数据？使用过队列，为什么要使用队列，何种情况下使用队列？

13.常用的队列有哪些？什么情况下使用？ 
BlockingQueue

14.线程的几种状态及其切换？ 
初始态 就绪态 运行态 阻塞态 终止态

15.创建线程池的方式，线程池一般配置多大？
public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
             Executors.defaultThreadFactory(), defaultHandler);
    }

Executors	
/**
 *创建固定数线程的线程池
 */
public static ExecutorService newFixedThreadPool(int nThreads) {
	return new ThreadPoolExecutor(nThreads, nThreads,
								  0L, TimeUnit.MILLISECONDS,
								  new LinkedBlockingQueue<Runnable>());
}

/**
 * 创建单个线程池
 */
 public static ExecutorService newSingleThreadExecutor() {
	return new FinalizableDelegatedExecutorService
		(new ThreadPoolExecutor(1, 1,
								0L, TimeUnit.MILLISECONDS,
								new LinkedBlockingQueue<Runnable>()));
}
	
public static ExecutorService newCachedThreadPool() {
	return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
								  60L, TimeUnit.SECONDS,
								  new SynchronousQueue<Runnable>());
}

public static ScheduledExecutorService newScheduledThreadPool(int corePoolSize) {
	return new ScheduledThreadPoolExecutor(corePoolSize);
}


16.怎样创建线程池？如果线上的任务量远大于线程池处理线程的量怎么用代码解决？

17.多线程使用Executors的方法还是？
ThreadPoolExecutor

18.解释线程池的各个参数，如果超过maxSize线程怎样的，小于coresize是怎样的？有使用哪些队列？线程处理有哪些机制？

19.不使用锁的情况下在同一个队列插入数据和查询数据怎样保证准确性？

# 集合
1.ArrayList LinkedList区别以及存储结构？
ArrayList:数组 查询快
LinkedList:链表结构 修改快

2.线程安全的集合有哪些？
Vector HashTable  ConcurrentHashMap

3.HashMap与ConcurrentHashMap有什么区别？HashMap的存储结构？ HashMap回答是hash结构存储会继续问如果相同的值要存储在指定位置冲突了怎么解决，冲突的取的时候怎么取保证数据准确

# JVM
1.对GC了解多少？

2.堆和栈的区别，堆中存放什么，栈中存放什么？

3.java导出dump的方式？
jmap -dump:live,format=b,file=heap.bin <pid>

4.jvm怎样加载class文件的，有哪些步骤，对象创建后存在哪里？静态常量值存在哪里？
加载->连接->验证->准备->初始化->卸载
对象创建后存在堆中
静态常量池存在方法区


5.java虚拟机内存怎么调整？
Xmx：最大堆内存
Xms：最小堆内存
xmn：年轻代大小

6.jvm中的垃圾收集算法有哪些？堆可以细分为哪些区域，什么对象会移到老年代，除了要被回收的对象移到老年代，其他什么情况直接移到老年代？

大对象直接移到老年代

7.半夜突然收到系统提示OOM，Eden快速使用了又释放，old全部耗尽了，怎样定位问题？

8. .class文件加载流程。怎样自定义类加载器？（笔试）

# spring
1.spring AOP IOC实现原理？

2.spring中的事务的传播方式怎样实现的？

3.spring中事务实现的原理？

4.springmvc实现原理？

5.springmvc controller线程安全吗？
安全，采用单利模式

6.springmvc中的拦截器的作用？

7.springmvc注解@pathVariable的作用？
将参数值作为请求路径的一部分

8.spring默认是单利还是多例的？ 
默认单例

9.springmvc流程
![输入图片说明](https://images.gitee.com/uploads/images/2019/1027/204226_d9850817_394236.png "屏幕截图.png")

具体步骤：

第一步：发起请求到前端控制器(DispatcherServlet)
第二步：前端控制器请求HandlerMapping查找 Handler （可以根据xml配置、注解进行查找）
第三步：处理器映射器HandlerMapping向前端控制器返回Handler，HandlerMapping会把请求映射为HandlerExecutionChain对象（包含一个Handler处理器（页面控制器）对象，多个HandlerInterceptor拦截器对象），通过这种策略模式，很容易添加新的映射策略
第四步：前端控制器调用处理器适配器去执行Handler
第五步：处理器适配器HandlerAdapter将会根据适配的结果去执行Handler
第六步：Handler执行完成给适配器返回ModelAndView
第七步：处理器适配器向前端控制器返回ModelAndView （ModelAndView是springmvc框架的一个底层对象，包括 Model和view）
第八步：前端控制器请求视图解析器去进行视图解析 （根据逻辑视图名解析成真正的视图(jsp)），通过这种策略很容易更换其他视图技术，只需要更改视图解析器即可
第九步：视图解析器向前端控制器返回View
第十步：前端控制器进行视图渲染 （视图渲染将模型数据(在ModelAndView对象中)填充到request域）
第十一步：前端控制器向用户响应结果
参考：https://www.cnblogs.com/leskang/p/6101368.html

10.aop应用场景？
 Authentication 权限
 Caching 缓存 
 Context passing 内容传递 
 Error handling 错误处理 
 Lazy loading　懒加载 
 Debugging　　调试 
 logging, tracing, profiling and monitoring　记录跟踪　
 优化　
 校准 
 Performance optimization 性能优化 
 Persistence　　 持久化 
 Resource pooling　 资源池 
 Synchronization　 同步 
 Transactions 事务 
 参考：https://blog.csdn.net/wuruijiang/article/details/78970720 
 权限认证、日志处理、异常处理

11.微服务权限怎么控制,用什么控制，是在zuul层控制还是每个子模块控制？
在网关层控制就可以了，token都是一样的

12.使用springboot的优缺点？
springboot特点：
a.Spring Boot 通过简单的步骤就可以创建一个 Spring 应用。
b.Spring Boot 为 Spring 整合第三方框架提供了开箱即用功能。
c.Spring Boot 的核心思想是约定大于配置。
简化开发模式，提高开发效率
亮点：
分别是自动装配、内嵌容器、应用监控、Starter 包简化框架集成难度。
自动装配：Spring Boot 会根据某些规则对所有配置的 Bean 进行初始化。可以减少了很多重复性的工作。比如使用 MongoDB 时，只需要在 pom.xml 中加入 MongoDB 的 Starter 包，然后配置 MongoDB 的连接信息，就可以直接使用 MongoTemplate 自动装配来操作数据库了。
内嵌容器：Spring Boot 应用程序可以不用部署到外部容器中，比如 Tomcat。Spring Boot 应用程序可以直接通过 Maven 命令编译成可执行的 jar 包，通过 java-jar 命令启动即可，非常方便。
应用监控：Spring Boot 中自带监控功能 Actuator，可以实现对程序内部运行情况进行监控，比如 Bean 加载情况、环境变量、日志信息、线程信息等。当然也可以自定义跟业务相关的监控，通过Actuator 的端点信息进行暴露。
Starter 包简化框架集成难度：将 Bean 的自动装配逻辑封装在 Starter 包内部，同时也简化了 Maven Jar 包的依赖，对框架的集成只需要加入一个 Starter 包的配置，降低了烦琐配置的出错几率。

13.springcloud的优缺点
优点：
第一个优点是服务独立，每个服务都有独立的代码仓库、独立的数据库、独立进行部署的能力。
第二点是开发体验好，开发体验好的关键点在于启动速度快，克隆代码速度快，编译部署速度快，技术选型更自由。即使用户服务用Spring Boot 1.X版本，积分服务用Spring Boot 2.X版本，服务之间也无任何影响。
第三点是职责专一性，服务只负责本身的业务，不关心无关业务。这样有利于实现不同的团队维护不同的服务。
第四点是按需扩容，也就是说某个服务特别耗内存，可以单独部署在内存比较大的机器上；如果特别耗 CPU , 那可以部署到 CPU 比较好的机器上。且只需要部署这个服务，不需要像单体应用那样部署整个应用。
缺点
当然，也不是说微服务就是完美的，在解决问题的同时也引入了新的问题。例如，分布式带来的复杂性，服务拆分后，本地调用变成了远程调用，服务实例有多个，如何负载均衡；被调用的服务出问题的话，如何调用容错；服务之间的依赖关系如何等问题。
再比如，运维的复杂性，拆分后的服务数量多，部署后的服务节点多，需要有日志的统一管理，才方便通过日志排查问题。服务需要有统一监控，才能在发生问题时及时告警。
还有，服务拆分的复杂性，如何拆分出对应的服务很关键，需要结合自身的业务领域进行合理的拆分。拆分后每个服务都有自己的数据库，当一个业务操作涉及多个服务时，如何保证数据一致性，等等

14.springboot启动有几种方式？
		a.main方法类
		b.java -jar
		c.spring-boot-plugin
		参考：https://blog.csdn.net/u011425751/article/details/79507386

15.springboot集成redis有几种方式？

16.eureka的心跳机制是什么？

17.feign底层怎么交互的？

18.分布式事务

参考：https://www.jianshu.com/p/ee4071d0c951
http://springcloud.cn/view/374

	常见方案：2PC TCC 本地消息事务  rocketmq消息处理

参考：https://hhbbz.github.io/2018/09/06/%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E5%8A%A1%E7%9A%84%E5%9B%9B%E7%A7%8D%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88/
http://server.it168.com/a2018/0623/3210/000003210898.shtml



12.springcloud之间的通信 
	feign

13.springcloud的部署是怎样的

14.springboot读取配置文件的方式？
@Value
@Autowired
Environment
@ConfigurationProperties


15.springcloud项目中用了哪些模块？
	eureka:注册中心
	feign:模块间通信
	ribbon:负载均衡
	hystrix:熔断
	zuul:网关路由
	
16.feign的实现原理？feign是否包含负载均衡？

17.springboot默认使用的服务器是哪个？
tomcat

18.springboot读取配置文件热加载机制是什么？

19.微服务中出现的问题？
	部署问题，增加运维部署压力
	分布式事务

20.springboot核心？
	starter
	自动装配
	springboot cli
	actuator


微服务中的日志怎么查看，比如A-->B-->C  A发现出了问题，怎样找到根源在C处及错误日志？

21.hystrix熔断原理？

22.日志的链路追踪怎么做的？

23.Struts2与springmvc区别

24.springboot starter的加载流程及自定义starter怎么实现？（简历中有些看过部分源码）

25.springboot 怎么加载tomcat？（看过相关的加载源码）

26.springboot自定义starter步骤。
 - a.创建 Starter 项目；
 - b.项目创建完后定义 Starter 需要的配置（Properties）类，比如数据-库的连接信息；
- c.编写自动配置类，自动配置类就是获取配置，根据配置来自动装配 Bean；
- d.编写 spring.factories 文件加载自动配置类，Spring 启动的时候会扫描 spring.factories 文件，指定文件中配置的类；
- e.编写配置提示文件 spring-configuration-metadata.json（不是必须的），在添加配置的时候，我们想要知道具体的配置项是什么作用，可以通过编写提示文件来提示；
- f.最后就是使用，在项目中引入自定义 Starter 的 Maven 依赖，增加配置值后即可使用。

# database
1.为什么要使用数据库索引，数据库索引有哪些，索引的底层原理是什么？

2.sql查询缓慢怎么处理，sql优化方案有哪些，explain用过吗？

3.数据库中的锁有几种？ 比如行锁、表锁等会深问

4.数据库为什么要使用事务，事务的原理？
保证原子性

5.oracle数据库的分页怎样实现？
使用伪表dual

6.数据库分库分表的方法，垂直分还是水平分，根据哪些来分？

7.union 与union all区别？

8.count(1) count(5) count(*)有什么区别，100万条数据的效率如何？

9.left join right join 区别？

10.inner join与join的区别？ 
无区别。

11.数据库设计遵循的三范式？

1NF:字段不可分; 
2NF:有主键，非主键字段依赖主键; 
3NF:非主键字段不能相互依赖; 

解释: 
1NF:原子性 字段不可再分,否则就不是关系数据库; 
2NF:唯一性 一个表只说明一个事物; 
3NF:每列都与主键有直接关系，不存在传递依赖;

12.mysql优化
	索引
	少使用order by
	少使用group by

13.mysql执行引擎
mysql 执行：show engines;
Engine            |Support|Comment                                                       |Transactions|XA |Savepoints|
------------------|-------|--------------------------------------------------------------|------------|---|----------|
FEDERATED         |NO     |Federated MySQL storage engine                                |            |   |          |
MRG_MYISAM        |YES    |Collection of identical MyISAM tables                         |NO          |NO |NO        |
MyISAM            |YES    |MyISAM storage engine                                         |NO          |NO |NO        |
BLACKHOLE         |YES    |/dev/null storage engine (anything you write to it disappears)|NO          |NO |NO        |
CSV               |YES    |CSV storage engine                                            |NO          |NO |NO        |
MEMORY            |YES    |Hash based, stored in memory, useful for temporary tables     |NO          |NO |NO        |
ARCHIVE           |YES    |Archive storage engine                                        |NO          |NO |NO        |
InnoDB            |DEFAULT|Supports transactions, row-level locking, and foreign keys    |YES         |YES|YES       |
PERFORMANCE_SCHEMA|YES    |Performance Schema                                            |NO          |NO |NO        |
数据库5.1之后默认使用InnoDB作为默认的引擎
innoDB特点：支持事务、行级锁、外键、XA、保存点
参考：https://www.jianshu.com/p/4bb9f78b4f6d

14.数据库的特性？ 
 ACID 原子性 一致性 隔离性 持久性

15.数据库的乐观锁和悲观锁的原理及使用？（version）

16.分库分表分页怎么查询？

分库分表根据什么去拆分，拆分后会产生什么问题？

17.千万条数据查询10s怎么去优化？

18.数据库分库分表怎么做？
如果使用sharding-jdbc框架则不用处理，否则

19.mysql分库分表用的什么插件还是自己写的算法实现？

20.图书管理系统该设计多少张表？

21.组合索引b c d    where cbd abd dca 是否走索引？
mysql组合索引具有最左匹配原则，上述组合都不具备最左匹配原则，所以都不走索引，如果where语句中都是等号则mysql通过优化后cbd还是走全部索引的

22.悲观锁、乐观锁的概念？

23.线上出现悲观锁了怎么处理？

24.mysql的索引结构是怎样的？

25.mysql创建索引和使用索引应该注意什么？




# mybatis
1.mybatis中#的区别？
#防止注入一般用于传入数据库对象，例如传入表名. 区别：#将传入的数据都当成一个字符串，会对自动传入的数据加一个双引号。如：order by #user_id#，如果传入的值是111,那么解析成sql时的值为order by "111", 如果传入的值是id，则解析成的sql为order by "id". 参考：http://blog.csdn.net/downkang/article/details/12499197/

2.mybatis的一级缓存 二级缓存的区别？ 
	一级缓存：默认开启
	二级缓存：默认不开启
参考：https://www.cnblogs.com/happyflyingpig/p/7739749.html

mybatis hibernate区别
hibernate：强大、方便、高效、复杂、间接、全自动化
mybatis：小巧、方便、高效、简单、直接、半自动化
参考：https://blog.csdn.net/qq_33688493/article/details/88988551

3.mybatis如果从oracle切到mysql怎么处理？
主要修改的地方，分页、表名

4.mybatis加载解析mapper查询数据库的流程？

# redis
1.redis的数据结构有哪些？ 
字符串(String)、列表(List)、散列(Hash)、集合(set）、排序集合（sort set）

2.缓存击穿、缓存穿透、缓存雪崩分别是什么，怎样解决？
	缓存穿透：查询一个一定不存在的数据，由于缓存是不命中时被动写的，并且出于容错考虑，如果从存储层查不到数据则不写入缓存，这将导致这个不存在的数据每次请求都要到存储层去查询，失去了缓存的意义。在流量大时，可能DB就挂掉了，要是有人利用不存在的key频繁攻击我们的应用，这就是漏洞。
	解决方案：有很多种方法可以有效地解决缓存穿透问题，最常见的则是采用布隆过滤器，将所有可能存在的数据哈希到一个足够大的bitmap中，一个一定不存在的数据会被 这个bitmap拦截掉，从而避免了对底层存储系统的查询压力。另外也有一个更为简单粗暴的方法（我们采用的就是这种），如果一个查询返回的数据为空（不管是数 据不存在，还是系统故障），我们仍然把这个空结果进行缓存，但它的过期时间会很短，最长不超过五分钟。
	缓存雪崩：在我们设置缓存时采用了相同的过期时间，导致缓存在某一时刻同时失效，请求全部转发到DB，DB瞬时压力过重雪崩。
	解决方案：缓存失效时的雪崩效应对底层系统的冲击非常可怕。大多数系统设计者考虑用加锁或者队列的方式保证缓存的单线 程（进程）写，从而避免失效时大量的并发请求落到底层存储系统上。这里分享一个简单方案就时讲缓存失效时间分散开，比如我们可以在原有的失效时间基础上增加一个随机值，比如1-5分钟随机，这样每一个缓存的过期时间的重复率就会降低，就很难引发集体失效的事件
	缓存击穿：对于一些设置了过期时间的key，如果这些key可能会在某些时间点被超高并发地访问，是一种非常“热点”的数据。这个时候，需要考虑一个问题：缓存被“击穿”的问题，这个和缓存雪崩的区别在于这里针对某一key缓存，前者则是很多key。缓存在某个时间点过期的时候，恰好在这个时间点对这个Key有大量的并发请求过来，这些请求发现缓存过期一般都会从后端DB加载数据并回设到缓存，这个时候大并发的请求可能会瞬间把后端DB压垮。

解决方案：尽量少的线程构建缓存(甚至是一个) + 数据一致性 + 较少的潜在危险
参考：https://www.cnblogs.com/raichen/p/7750165.html
https://juejin.im/post/5dbef8306fb9a0203f6fa3e2
3.redis的应用，redis是否有事务
	缓存 分布式锁
	分布式锁的实现方式：
	setex expire 
使用场景：
- a.记录帖子的点赞数、评论数和点击数 (hash)。
- b.记录用户的帖子 ID 列表 (排序)，便于快速显示用户的帖子列表 (zset)。
- c.记录帖子的标题、摘要、作者和封面信息，用于列表页展示 (hash)。
- d.记录帖子的点赞用户 ID 列表，评论 ID 列表，用于显示和去重计数 (zset)。
- e.缓存近期热帖内容 (帖子内容空间占用比较大)，减少数据库压力 (hash)。
- f.记录帖子的相关文章 ID，根据内容推荐相关帖子 (list)。
- g.如果帖子 ID 是整数自增的，可以使用 Redis 来分配帖子 ID(计数器)。
- h.收藏集和帖子之间的关系 (zset)。
- i.记录热榜帖子 ID 列表，总热榜和分类热榜 (zset)。
- j.缓存用户行为历史，进行恶意行为过滤 (zset,hash)。
	
4.redis常用的淘汰策略
volatile-lru
allKeys-lru
volatile-ttl
volatile-random
allKeys-random
volatile-lfu
allKeys-lfu
noeviction 不淘汰策略，超过最大内存，返回错误信息



# 搜索框架Elasticsearch solr
1.elasticsearch索引怎么设计？
	
2.elasticsearch solr 底层基于什么实现
	lucene,数据结构是倒排序索引，
	举例说明：正常我们查询某些词都是先从书本中查找，倒排序索引是我们把书中的词获取后将含有该词的书本记录
	Elasticsearch 与 Solr 的比较总结
		二者安装都很简单；
		Solr 利用 Zookeeper 进行分布式管理，而 Elasticsearch 自身带有分布式协调管理功能;
		Solr 支持更多格式的数据，而 Elasticsearch 仅支持json文件格式；
		Solr 官方提供的功能更多，而 Elasticsearch 本身更注重于核心功能，高级功能多有第三方插件提供；
		Solr 在传统的搜索应用中表现好于 Elasticsearch，但在处理实时搜索应用时效率明显低于 Elasticsearch。
		Solr 是传统搜索应用的有力解决方案，但 Elasticsearch 更适用于新兴的实时搜索应用。
	参考：https://www.cnblogs.com/pc-boke/articles/9851521.html
	
3.solr搜索实现原理、使用的排序算法是什么，怎样实现快速查询？
倒排序索引

4.elasticsearch底层索引怎么设计？
	基于lucene 倒排序索引

# Linux
1.服务器查询某个时间段的包含"service"的日志用什么命令？
	sed -n '/2018-10-10 10:10:29/,/2018-10-10 10:20:29/p' log文件位置

2.服务器查看服务端口的命令？
netstat -anp


3.怎样查找某个端口号被占用？
lsof -i:port

4.怎样关闭某个端口或者进程？
netstat -anl | grep tomcat
kill -9 进程id

5.linux给文件夹授权的命令？
chmod -r dir

.nginx负载均衡怎么配置？
upstream backserver {
	server 192.168.2.12:8001;
	server 192.168.2.13:8002;
}

最简单的配置：
http {
    upstream myapp1 {
        server srv1.example.com;
        server srv2.example.com;
        server srv3.example.com;
    }

    server {
        listen 80;

        location / {
            proxy_pass http://myapp1;
        }
    }
}

最少连接负载均衡
upstream myapp1 {
        least_conn;
        server srv1.example.com;
        server srv2.example.com;
        server srv3.example.com;
    }
	
	ip_hash方式
	upstream myapp1 {
    ip_hash;
    server srv1.example.com;
    server srv2.example.com;
    server srv3.example.com;
}

根据权重配置：
upstream myapp1 {
        server srv1.example.com weight=3;
        server srv2.example.com;
        server srv3.example.com;
    }
	
	参考：
	http://nginx.org/en/docs/http/load_balancing.html
	https://blog.csdn.net/qq_35119422/article/details/81505732
	
	
6.jenkins的部署流程是怎样的？
git拉取代码
maven编译
tomcat部署

7.linux分组授权命令


# 设计

1.设计数据库的时候会考虑哪些因素，怎样去建表？

2.对于建表，大部分表中哪些相同的字段会都考虑要？
 id、添加时间、修改时间、添加人，修改人等
	
3.万人聊天系统怎么设计？可以查资料，高并发问题，序列化问题？长连接？
	
4.千万级的数据高并发入库怎么设计？

5.5张表、每个表5列，每张表数据3000w，库存表数据怎样能快速查询出来，怎样

6.防刷短信验证码，用户登录的时候，发现大量不同的ip不同的手机号用验证码登录，怎样防止大量验证码盗刷

7.高并发场景下怎样保证缓存和DB的数据一致 提示：比如用redis缓存，更新操作是先删缓存更新数据库，还是更新数据库了再更新缓存，会出现什么问题
参考：
![enter description here](./images/1574653548839.png)

8.怎样保证幂等，你们怎么做的？
数据库可以使用状态

9.客户端数据怎样保证安全性，调用接口之前？
 比如身份证号码，手机号调用接口之前怎样保证用户隐私，我说了对称加密，他问客户端怎样拿密钥，没答上来

10.怎样防止数据被篡改，你知道的防数据篡改的方式有哪些


11.如果让你对外开发一个接口，你会考虑哪些因素？
数据安全 并发数  

12.设计一个秒杀系统。（yy两轮技术问到）

13.直播系统，500w个钻，有100 50 20 0，主播点击按钮之后用户可以获取随机的钻，根据用户获取的钻的数量调整概率尽量保证公平，实现高可用、可扩展的系统设计。（yy笔试及面试）

14.类似mybatis的框架你怎么设计？

后面有手写文件复制的代码和括号匹配的代码

金融的会问你怎样设计一个抢红包的程序 在群聊中 聊天记录有5亿条你存在一张表吗？ 存一张表肯定不行，按时间去存和按用户id存 本人最后回答根据时间段和记录数来使用文件存储，数据库中存储文件名和时间等唯一标识

项目架构
		springcloud+mysql集群+redis集群+elasticsearch集群+apollo集群+tomcat集群	

线上是否使用docker部署过项目？

rabbitmq exchange干嘛的？
将生产的消息分发给队列


面试通关：http://www.spring4all.com/article/716
