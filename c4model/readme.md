### 说明
当前目录下的c4.js来源于[c4model原始地址](https://tobiashochguertel.github.io/c4-draw.io/c4.js),由于浏览器安全限制无法访问将文件复制到本地仓库，iodraw和drawio的添加方式都是一样的，将该地址添加到插件对应的url即可，或者添加到自己的仓库网络能访问也可以