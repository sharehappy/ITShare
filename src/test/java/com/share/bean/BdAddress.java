package com.share.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by hujinhao on 2019/7/22.
 */
@Getter
@Setter
public class BdAddress {
    private String city;
    private String code;
    private String country;
    private String creationtime;
    private String creator;
    private Integer dataoriginflag;
    private String detailinfo;
    private String modifiedtime;
    private String modifier;
    private String pkAddress;
    private String postcode;
    private String province;
    private String ts;
    private String vsection;


}
