package com.share.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * 部门主表
 * Created by hujinhao on 2019/7/22.
 */
@Getter
@Setter
public class OrgDept {
    private String address;
    private String code;
    private String createdate;
    private String creationtime;
    private String creator;
    private Integer dataoriginflag;
    private String def1;
    private String def10;
    private String def11;
    private String def12;
    private String def13;
    private String def14;
    private String def15;
    private String def16;
    private String def17;
    private String def18;
    private String def19;
    private String def2;
    private String def3;
    private String def4;
    private String def5;
    private String def6;
    private String def7;
    private String def8;
    private String def9;
    private String deptcanceldate;
    private Integer depttype;
    private Integer displayorder;
    private Integer enablestate;
    private Character hrcanceled;
    private String innercode;
    private Character islastversion;
    private Character isretail;
    private String memo;
    private String mnecode;
    private String modifiedtime;
    private String modifier;
    private String name;
    private String name2;
    private String name3;
    private String name4;
    private String name5;
    private String name6;

    private String pkDept;
    private String pkFatherorg;
    private String pkGroup;
    private String pkOrg;
    private String pkVid;

    private String principal;
    private String resposition;
    private String shortname;
    private String shortname2;
    private String shortname3;
    private String shortname4;
    private String shortname5;
    private String shortname6;
    private String tel;
    private String ts;
    private String venddate;
    private String vname;
    private String vname2;
    private String vname3;
    private String vname4;
    private String vname5;
    private String vname6;
    private String vno;
    private String vstartdate;
    private String chargeleader;
    private String deptlevel;
    private Character orgtype13;
    private Character orgtype17;

}
