package com.share.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * 国家地区
 * Created by hujinhao on 2019/7/22.
 */
@Getter
@Setter
public class BdCountryzone {

    private String code;
    private String codeth;
    private String creationtime;
    private String creator;
    private Integer dataoriginflag;
    private String description;
    private String modifiedtime;
    private String modifier;
    private String name;
    private String name2;
    private String name3;
    private String name4;
    private String name5;
    private String name6;
    private String phonecode;
    private String pkCountry;
    private String pkFormat;
    private String pkLang;
    private String pkOrg;
    private String pkTimezone;
    private String ts;
    private String wholename;
    private String wholename2;
    private String wholename3;
    private String wholename4;
    private String wholename5;
    private String wholename6;
    private String bbanrule;
    private String ename;
    private Integer ibanlength;
    private String ibanrule;
    private Character iseucountry;
    private String pkCurrtype;

}
