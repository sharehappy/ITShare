package com.share.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by hujinhao on 2019/7/29.
 */
@Getter
@Setter
public class BdPsndoc {
    private String addr;
    private String birthdate;
    private String code;
    private String creationtime;
    private String creator;
    private Integer dataoriginflag;
    private String def1;
    private String def10;
    private String def11;
    private String def12;
    private String def13;
    private String def14;
    private String def15;
    private String def16;
    private String def17;
    private String def18;
    private String def19;
    private String def2;
    private String def20;
    private String def3;
    private String def4;
    private String def5;
    private String def6;
    private String def7;
    private String def8;
    private String def9;
    private String email;
    private Integer enablestate;
    private String firstname;
    private String homephone;
    private String id;
    private String idtype;
    private String joinworkdate;
    private String lastname;
    private String mnecode;
    private String mobile;
    private String modifiedtime;
    private String modifier;
    private String name;
    private String name2;
    private String name3;
    private String name4;
    private String name5;
    private String name6;
    private String nickname;
    private String officephone;
    private String pkGroup;
    private String pkOrg;
    private String pkPsndoc;
    private Integer sex;
    private String ts;
    private String usedname;
    private Character isshopassist;
}
