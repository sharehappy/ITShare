package com.share.bean;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Created by hujinhao on 2019/8/2.
 */
@Getter
@Setter
public class BdCustomer {
    private String code;
    private String corpaddress;
    private String creationtime;
    private String creator;
    private Integer custprop;
    private Integer custstate;
    private Integer dataoriginflag;
    private String def1;
    private String def10;
    private String def11;
    private String def12;
    private String def13;
    private String def14;
    private String def15;
    private String def16;
    private String def17;
    private String def18;
    private String def19;
    private String def2;
    private String def20;
    private String def21;
    private String def22;
    private String def23;
    private String def24;
    private String def25;
    private String def26;
    private String def27;
    private String def28;
    private String def29;
    private String def3;
    private String def30;
    private String def4;
    private String def5;
    private String def6;
    private String def7;
    private String def8;
    private String def9;
    private Integer deletestate;
    private String delperson;
    private String deltime;
    private String ecotypesincevfive;
    private String email;
    private Integer enablestate;
    private String ename;
    private String fax1;
    private String fax2;
    private Character frozenflag;
    private Character isfreecust;
    private Character isretailstore;
    private Character issupplier;
    private Character isvat;
    private String legalbody;
    private String memo;
    private String mnecode;
    private String modifiedtime;
    private String modifier;
    private String name;
    private String name2;
    private String name3;
    private String name4;
    private String name5;
    private String name6;
    private String pkAreacl;
    private String pkBilltypecode;
    private String pkCountry;
    private String pkCurrtype;
    private String pkCustclass;
    private String pkCustomer;
    private String pkCustomerMain;
    private String pkCustomerpf;
    private String pkCusttaxes;
    private String pkFinanceorg;
    private String pkFormat;
    private String pkGroup;
    private String pkOrg;
    private String pkSupplier;
    private String pkTimezone;
    private BigDecimal registerfund;
    private String shortname;
    private String taxpayerid;
    private String tel1;
    private String tel2;
    private String tel3;
    private String trade;
    private String ts;
    private String url;
    private String vatcode;

}
