package com.share.constant;

import lombok.Getter;

/**
 * author:caifan
 * date:2019/11/21
 */
@Getter
public enum  StatusEnum {
    SUCCESS("1000", "成功"),
    FAILED("1001", "失败")
    ;

    private String code;
    private String desc;

    StatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
