package com.share.tool;

import com.share.tools.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * @desc:
 * @author:caifan
 * @date:2022/5/16
 */
public class FileUtilTest {

    @Test
    public void zipDirTest() {
        FileUtils.zipDir("D:/util", "D:/util.zip");
    }

    @Test
    public void zipInputstreamTest() {
        String a = "hello";
        String b = "fdafwefafa";
        Map<String, InputStream> map = new HashMap<>();
        map.put("a.txt", IOUtils.toInputStream(a, Charset.defaultCharset()));
        map.put("b.txt", IOUtils.toInputStream(b, Charset.defaultCharset()));
        FileUtils.zipInputStream(map, "D:/test.zip");
    }
}
