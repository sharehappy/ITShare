package com.share.tools.svn;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.net.URL;
import java.util.stream.Stream;

/**
 * @author caifan
 * @date 2020/2/28
 */
public class SVNMain {
    private static final String ROOT_DIR = "E:/code";//代码的根目录
    public static void main(String[] args) throws Exception {
        /*InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("svn-config.properties");
        Properties properties = new Properties();
        properties.load(in);*/
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("svn-config.properties");
        URL url = Thread.currentThread().getContextClassLoader().getResource("projectName.txt");
        File file = new File(url.toURI());
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        Stream<String> lines = reader.lines();
        lines.forEach(line -> {
            if (StringUtils.isNotEmpty(line.trim()) && !line.trim().startsWith("#")) {
                String projectName = line.trim();
                SvnConfig svnConfig = SvnConfig.getInstance();
                svnConfig.setSourceProject(projectName);
                String checkoutDir = ROOT_DIR + File.separator + projectName;
                File fileDir = new File(checkoutDir);
                if (!fileDir.exists()) {
                    fileDir.mkdirs();
                }
                svnConfig.setSourceCheckOutDir(checkoutDir);
                SvnService svnService = new SvnService();
                svnService.checkOut(svnConfig);
            }
        });

    }
}
