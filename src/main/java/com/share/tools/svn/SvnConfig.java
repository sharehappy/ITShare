package com.share.tools.svn;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author caifan
 * @date 2020/2/28
 */
@Data
@Slf4j
public class SvnConfig {
    private String sourceSvn;

    private String sourceProject;

    private String sourceSvnUser;

    private String sourceSvnPassword;

    private String sourceCheckOutDir;

    private SvnConfig() {
        try {
            /*final String jarPath = System.getProperty("user.dir");
            final String propertiesPath = jarPath + File.separator + "conf/svn-sync.properties";
            final InputStream in = new FileInputStream(new File(propertiesPath));*/
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("svn/svn-config.properties");
            final Properties properties = new Properties();
            properties.load(is);
            this.sourceSvn = properties.getProperty("sourceSvn").trim();
//            this.sourceProject = properties.getProperty("sourceProject").trim();
            this.sourceSvnUser = properties.getProperty("sourceSvnUser").trim();
            this.sourceSvnPassword = properties.getProperty("sourceSvnPassword").trim();
//            this.sourceCheckOutDir = properties.getProperty("sourceCheckOutDir").trim();
        } catch (IOException e) {
            log.error("SvnConfig struct SvnConfig() is error: ", e);
        }
    }

    private static final SvnConfig instance = new SvnConfig();

    public static SvnConfig getInstance() {
        return instance;
    }
}
