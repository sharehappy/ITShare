package com.share.tools;

/**
 * @author caifan
 * @date 2019/10/20
 */
public class SortUtils {

    /**
     * 冒泡排序
     * @param arrays
     * @param flag 升序降序标识  默认true升序
     */
    public static void bubbleSort(int[] arrays, boolean flag) {
        int tmp;
        for (int i=0;i<arrays.length;i++) {
            for (int j=i+1;j<arrays.length;j++) {
                /*
                 * 升序
                 */
                if (flag) {
                    if (arrays[i] > arrays[j]) {
                        tmp = arrays[i];
                        arrays[i] = arrays[j];
                        arrays[j] = tmp;
                    }
                } else {
                    if (arrays[i] < arrays[j]) {
                        tmp = arrays[i];
                        arrays[i] = arrays[j];
                        arrays[j] = tmp;
                    }
                }
            }
        }
    }
}
