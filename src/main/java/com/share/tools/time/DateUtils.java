package com.share.tools.time;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author caifan
 * @date 2019/10/20
 */
public class DateUtils {
    public static final String PARTTERN_YMDHMS = "yyyy-mm-dd HH:mm:ss";

    /**
     * 根据指定格式获取当前时间字符串
     * @param parttern
     * @return
     */
    public static String getCurrentTime(String parttern) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(parttern);
        return localDateTime.format(formatter);
    }

    public static String date2String(Date date, String parttern) {
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(parttern);
    }

    /**
     * 字符串转Date
     * @param time
     * @param parttern
     * @return
     */
    public static Date str2Date(String time, String parttern) {
        org.joda.time.format.DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(parttern);
        return DateTime.parse(time, dateTimeFormatter).toDate();
    }

}
