package com.share.tools.security;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import javax.crypto.Cipher;
import java.io.*;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import java.util.Base64;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * RSA 非对称、公钥加密,签名验签：基于数学函数
 * 公钥(n,e)加密: m^e=c(mod n)，m明文，c密文
 * 私钥(n,d)解密: c^d=m(mod n)，c密文，m明文
 *
 * @author caifan
 * @date 2019/10/19
 */
@Slf4j
public class RsaUtils {

    public static final String KEY_ALGORITHM = "RSA";

    /**
     * 算法常量
     */
    public static final String SIGN_ALGORITHM_SHA256RSA = "SHA256withRSA";

    /**
     * RSA Ecb模式 公钥加密
     *
     * @param publicKey 公钥
     * @param data      明文
     * @param padMode   填充模式
     * @return 密文
     */
    public static byte[] rsaEcbEncrypt(RSAPublicKey publicKey, byte[] data, String padMode) {
        //
        String algorithm = "RSA/ECB/" + padMode;
        byte[] res = null;
        if (publicKey == null) {
            log.error("publicKey is null");
        }
        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            res = cipher.doFinal(data);
        } catch (Exception e) {
            log.error("Fail: RSA Ecb Encrypt", e);
        }
        return res;
    }

    /**
     * RSA Ecb 私钥解密
     *
     * @param privateKey 私钥
     * @param data       密文
     * @param padMode    填充模式
     * @return 明文
     */
    public static byte[] rsaEcbDecrypt(RSAPrivateKey privateKey, byte[] data, String padMode) {
        if (privateKey == null) {
            log.error("privateKey is null");
        }
        String algorithm = "RSA/ECB/" + padMode;
        byte[] res = null;
        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            res = cipher.doFinal(data);
        } catch (Exception e) {
            log.error("Fail: RSA Ecb Decrypt", e);
        }
        return res;
    }

    /**
     * RSA摘要  私钥签名
     *
     * @param privateKey 私钥
     * @param data       消息
     * @return 签名
     */
    public static byte[] signRsa(RSAPrivateKey privateKey, byte[] data) {
        byte[] result = null;
        Signature st;
        try {
            st = Signature.getInstance(KEY_ALGORITHM);
            st.initSign(privateKey);
            st.update(data);
            result = st.sign();
        } catch (Exception e) {
            log.error("Fail: RSA  sign", e);
        }
        return result;
    }

    /**
     * RSA Sha256摘要  私钥签名
     *
     * @param privateKey 私钥
     * @param data       消息
     * @return 签名
     */
    public static byte[] signWithSha256(RSAPrivateKey privateKey, byte[] data) {
        byte[] result = null;
        Signature st;
        try {
            st = Signature.getInstance(SIGN_ALGORITHM_SHA256RSA);
            st.initSign(privateKey);
            st.update(data);
            result = st.sign();
        } catch (Exception e) {
            log.error("Fail: RSA with sha256 sign", e);
        }
        return result;
    }

    /**
     * RSA Sha256摘要  公钥验签
     *
     * @param publicKey 公钥
     * @param data      消息
     * @param sign      签名
     * @return 验签结果
     */
    public static boolean verifyWithSha256(RSAPublicKey publicKey, byte[] data, byte[] sign) {
        boolean correct = false;
        try {
            Signature st = Signature.getInstance(SIGN_ALGORITHM_SHA256RSA);
            st.initVerify(publicKey);
            st.update(data);
            correct = st.verify(sign);
        } catch (Exception e) {
            log.error("Fail: RSA with sha256 verify", e.getMessage());
        }
        return correct;
    }

    /**
     * RSA 摘要  公钥验签
     *
     * @param publicKey 公钥
     * @param data      消息
     * @param sign      签名
     * @return 验签结果
     */
    public static boolean verifyRsa(RSAPublicKey publicKey, byte[] data, byte[] sign) {
        boolean correct = false;
        try {
            Signature st = Signature.getInstance(KEY_ALGORITHM);
            st.initVerify(publicKey);
            st.update(data);
            correct = st.verify(sign);
        } catch (Exception e) {
            log.error("Fail: RSA verify", e.getMessage());
        }
        return correct;
    }

    public static PublicKey getPublicKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = Base64.getDecoder().decode(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return publicKey;
    }

    public static PrivateKey getPrivateKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = Base64.getDecoder().decode(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }

    public static String sign(Map param, String key, String signType) throws Exception {
        RSAPrivateKey privateKey = (RSAPrivateKey) getPrivateKey(key);
        TreeMap<String, String> treeMap = new TreeMap<>();
        treeMap.putAll(param);
        treeMap.remove("digest");
        String signStr = getParamStr(treeMap);
        byte[] msg = signStr.getBytes("UTF-8");
        byte[] signature = null;
        if ("RSA".equals(signType)) {
            signature = signRsa(privateKey, msg);
        } else if ("RSA2".equals(signType)) {
            signature = signWithSha256(privateKey, msg);
        }
        return Base64.getEncoder().encodeToString(signature);
    }

    public static boolean verify(Map param, String key, String signType, String sign) throws Exception {
        RSAPublicKey publicKey = (RSAPublicKey) getPublicKey(key);
        TreeMap<String, String> treeMap = new TreeMap<>();
        treeMap.putAll(param);
        treeMap.remove("digest");
        String signStr = getParamStr(treeMap);
        byte[] msg = signStr.getBytes("UTF-8");
        byte[] signB = Base64.getDecoder().decode(sign);
        if ("RSA".equals(signType)) {
            return verifyRsa(publicKey, msg, signB);
        } else if ("RSA2".equals(signType)) {
            return verifyWithSha256(publicKey, msg, signB);
        }
        return false;
    }

    private static String getParamStr(TreeMap<String, String> paramsMap) {
        StringBuilder param = new StringBuilder();
        for (Iterator<Map.Entry<String, String>> it = paramsMap.entrySet()
                .iterator(); it.hasNext(); ) {
            Map.Entry<String, String> e = it.next();
            if (StringUtils.isBlank(e.getValue())) {
                continue;
            }
            param.append(e.getKey()).append("=")
                    .append(e.getValue()).append("&");
        }
        return param.toString().substring(0, param.toString().length() - 1);
    }

    private static PrivateKey getPrivateKeyFromFile(String path) {
        InputStream in = null;
        try {
            in = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String readLine = null;
            while ((readLine = br.readLine()) != null) {
                if (readLine.charAt(0) == '-') {
                    continue;
                } else {
                    sb.append(readLine);
                    sb.append('\r');
                }
            }
            PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(sb.toString()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey priKey = keyFactory.generatePrivate(priPKCS8);
            return priKey;
        } catch (IOException e) {
            log.error("读取私钥文件失败！", e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            log.error("获取私钥失败！", e.getMessage());
        } catch (InvalidKeySpecException e) {
            log.error("获取私钥失败！", e.getMessage());
        }
        return null;
    }

    public static PublicKey getPublicKeyFromFile(String path) {
        InputStream in = null;
        try {
            in = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String readLine = null;
            while ((readLine = br.readLine()) != null) {
                if (readLine.charAt(0) == '-') {
                    continue;
                } else {
                    sb.append(readLine);
                    sb.append('\r');
                }
            }
            X509EncodedKeySpec pubX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(sb.toString()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(pubX509);
        } catch (IOException e) {
            log.error("读取公钥文件失败！", e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            log.error("获取公钥失败！", e.getMessage());
        } catch (InvalidKeySpecException e) {
            log.error("获取公钥失败！", e.getMessage());
        }
        return null;

    }

    private static String getkeyStrFromFile(String path) {
        InputStream in = null;
        try {
            in = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String readLine = null;
            while ((readLine = br.readLine()) != null) {
                if (readLine.charAt(0) == '-') {
                    continue;
                } else {
                    sb.append(readLine);
//					sb.append('\r');
                }
            }
            return sb.toString();
        } catch (IOException e) {
            log.error("读取私钥文件失败！", e.getMessage());
        }
        return null;
    }

}
