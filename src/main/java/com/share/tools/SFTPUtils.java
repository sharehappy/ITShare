package com.share.tools;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author caifan
 * @date 2019/10/19
 */
@Slf4j
public class SFTPUtils {

    public static ChannelSftp getChannelSftp(String host, int port, String username, String password) {
        ChannelSftp channelSftp = null;
        JSch jSch = null;
        try {
            jSch = new JSch();
            Session sshSession = jSch.getSession(username, host, port);
            sshSession.setPassword(password);
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            sshSession.setConfig(sshConfig);
            sshSession.connect();
            log.info("sshSession connecting...");
            Channel channel = sshSession.openChannel("sftp");
            channel.connect();
            log.info("channel connected");
            channelSftp = (ChannelSftp) channel;
            log.info("connected to host {} port {}", host, port);
        } catch (Exception e) {
            log.error("getChannelSftp error", e);
        }
        return channelSftp;
    }

    /**
     * 获取ZipInputStream的数据装换为字符串
     * @param zipInputStream
     * @return
     */
    public static String toString(ZipInputStream zipInputStream){
        String result = null;
        ZipEntry zipEntry = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            if (null != zipInputStream) {
                while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                    byteArrayOutputStream = new ByteArrayOutputStream();
                    int len;
                    byte[] bytes = new byte[1024];
                    while ((len = zipInputStream.read(bytes)) != -1) {
                        byteArrayOutputStream.write(bytes, 0, len);
                    }
                    InputStream inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                    result = IOUtils.toString(inputStream);
                }
            }else {
                log.warn("zipInputStream is null");
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e){
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (null != byteArrayOutputStream){
                    byteArrayOutputStream.flush();
                    byteArrayOutputStream.close();
                }
                if (null != zipInputStream){
                    zipInputStream.close();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        return result;
    }
}
