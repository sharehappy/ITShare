package com.share.tools;

import com.share.domain.MetaType;
import org.apache.commons.lang3.builder.ToStringBuilder;

import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 参考此类可以进行树形结构操作
 * @author caifan
 * @date 2019/10/20
 */
public class RecursionUtils {
    public static List<MetaType> metaTypeList;

    static {
        metaTypeList = allMetaTypeList();
    }

    public static List<MetaType> allMetaTypeList() {
        List<MetaType> metaTypes = new ArrayList<>();
        MetaType metaType1 = new MetaType();
        metaType1.setId(1L);
        metaType1.setMetaKey("300000");
        metaType1.setMetaValue("日志1");
        metaType1.setType("logType");
        MetaType metaType2 = new MetaType();
        metaType2.setId(2L);
        metaType2.setMetaKey("100000");
        metaType2.setMetaValue("日志2");
        metaType2.setType("logType");
        MetaType metaType3 = new MetaType();
        metaType3.setId(3L);
        metaType3.setMetaKey("200000");
        metaType3.setMetaValue("日志3");
        metaType3.setType("logType");
        MetaType metaType4 = new MetaType();
        metaType4.setId(7L);
        metaType4.setMetaKey("310000");
        metaType4.setMetaValue("日志31");
        metaType4.setType("logType");
        metaType4.setParentId(1L);
        MetaType metaType5 = new MetaType();
        metaType5.setId(12L);
        metaType5.setMetaKey("3120000");
        metaType5.setMetaValue("日志32");
        metaType5.setType("logType");
        metaType5.setParentId(1L);
        MetaType metaType6 = new MetaType();
        metaType6.setId(8L);
        metaType6.setMetaKey("210000");
        metaType6.setMetaValue("日志21");
        metaType6.setType("logType");
        metaType6.setParentId(2L);
        metaTypes.add(metaType1);
        metaTypes.add(metaType6);
        metaTypes.add(metaType2);
        metaTypes.add(metaType3);
        metaTypes.add(metaType4);
        metaTypes.add(metaType5);
        return metaTypes;
    }

    @Test
    public void treeTest() {
        System.out.println(ToStringBuilder.reflectionToString(metaType(1L)));
    }


    public MetaType metaType(Long id) {
        MetaType metaType = metaTypeList.stream().filter(meta -> meta.getId().equals(id)).collect(Collectors.toList()).get(0);
        Map<Long, List<MetaType>> collect = metaTypeList.stream().filter(type -> type.getParentId() != null).collect(Collectors.groupingBy(MetaType::getParentId));
        if (collect != null && collect.size() > 0) {
            recursion(collect, metaType);
        }
        return metaType;
    }

    private MetaType recursion(Map<Long, List<MetaType>> maps, MetaType metaType) {
        if (metaType.getMetaTypeList() == null) {
            if (maps.get(metaType.getId()) != null) {
                metaType.setMetaTypeList(maps.get(metaType.getId()));
                maps.remove(metaType.getId());
                if (maps.size() > 0) {
                    recursion(maps, metaType);
                }
            }
        } else {
            List<MetaType> metaTypeList = metaType.getMetaTypeList();
            if (metaTypeList != null && metaTypeList.size() > 0) {
                for (MetaType meta : metaTypeList) {
                    recursion(maps, meta);
                }
            }
        }
        return metaType;
    }
}
