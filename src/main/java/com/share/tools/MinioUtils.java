package com.share.tools;

import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;

/**
 * @desc: minio上传工具类
 * @author:caifan
 * @date:2020/5/16
 */
@Slf4j
public class MinioUtils {
    private static String minioUrl;
    private static String minioName;
    private static String minioPass;
    private static String bucketName;

    public static void setMinioUrl(String minioUrl) {
        MinioUtils.minioUrl = minioUrl;
    }

    public static void setMinioName(String minioName) {
        MinioUtils.minioName = minioName;
    }

    public static void setMinioPass(String minioPass) {
        MinioUtils.minioPass = minioPass;
    }

    public static void setBucketName(String bucketName) {
        MinioUtils.bucketName = bucketName;
    }

    private static MinioClient minioClient = null;

    /**
     * 上传文件
     * @param file
     * @return
     */
    public static String upload(MultipartFile file, String bizPath) {
        String file_url = "";
        try {
            minioClient = initMinio(minioUrl, minioName,minioPass);
            // 检查存储桶是否已经存在
            if(minioClient.bucketExists(bucketName)) {
                log.info("Bucket already exists.");
            } else {
                // 创建一个名为ota的存储桶
                minioClient.makeBucket(bucketName);
                log.info("create a new bucket.");
            }
            InputStream stream = file.getInputStream();
            String orgName = file.getOriginalFilename();// 获取文件名
            String objectName;
            if (StringUtils.isEmpty(bizPath)) {
                objectName = orgName.substring(0, orgName.lastIndexOf(".")) + "_" + System.currentTimeMillis() + orgName.substring(orgName.indexOf("."));
            } else {
                objectName = bizPath + "/" + orgName.substring(0, orgName.lastIndexOf(".")) + "_" + System.currentTimeMillis() + orgName.substring(orgName.indexOf("."));
            }

            // 使用putObject上传一个本地文件到存储桶中。
//            minioClient.putObject(bucketName,objectName, stream,stream.available(),"application/octet-stream");//6.x
            // 7.x这里的两个参数看看源码
            PutObjectOptions objectOptions = new PutObjectOptions(stream.available(), 10 * 1024 * 1024);
            minioClient.putObject(bucketName, objectName, stream, objectOptions);
            stream.close();
            file_url = minioUrl+bucketName+"/"+objectName;
        }catch (IOException e){
            log.error(e.getMessage(), e);
        } catch (InvalidKeyException e) {
            log.error(e.getMessage(), e);
        } catch (InsufficientDataException e) {
            log.error(e.getMessage(), e);
        } catch (InvalidResponseException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return file_url;
    }

    /**
     * 初始化客户端
     * @param minioUrl
     * @param minioName
     * @param minioPass
     * @return
     */
    private static MinioClient initMinio(String minioUrl, String minioName,String minioPass) {
        if (minioClient == null) {
            try {
                minioClient = new MinioClient(minioUrl, minioName,minioPass);
            } catch (InvalidEndpointException e) {
                e.printStackTrace();
            } catch (InvalidPortException e) {
                e.printStackTrace();
            }
        }
        return minioClient;
    }
}
