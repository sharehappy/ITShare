package com.share.config;

/**
 * @desc: minio配置
 * @author:caifan
 * @date:2020/5/16
 */

import com.share.tools.MinioUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinioConfig {

    @Value(value = "${cms.minio.minio_url}")
    private String minioUrl;
    @Value(value = "${cms.minio.minio_name}")
    private String minioName;
    @Value(value = "${cms.minio.minio_pass}")
    private String minioPass;
    @Value(value = "${cms.minio.bucketName}")
    private String bucketName;

    @Bean
    public void initMinio(){
        if(!minioUrl.startsWith("http")){
            minioUrl = "http://" + minioUrl;
        }
        if(!minioUrl.endsWith("/")){
            minioUrl = minioUrl.concat("/");
        }
        MinioUtils.setMinioUrl(minioUrl);
        MinioUtils.setMinioName(minioName);
        MinioUtils.setMinioPass(minioPass);
        MinioUtils.setBucketName(bucketName);
    }
}
