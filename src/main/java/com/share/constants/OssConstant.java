package com.share.constants;

/**
 * @author caifan
 * @since 2022/9/1
 */
public interface OssConstant {

    /**
     * oss 商户图片信息
     */
    String OSS_VENDOR_DIR = "vendor";

    String FILE_SEPERATOR = "/";

    /**
     * oss请求头
     */
    String OSS_PREFIX = "https://";

    /**
     * oss endpoint
     */
    String OSS_ENDPOINT_PREFIX = "oss-cn-hangzhou.aliyuncs.com";

    String DOT = ".";
}
