package com.share.domain;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author caifan
 * @date 2019/10/20
 */
@Data
@ToString
public class Person implements Serializable {
    private Integer id;
    private Date createDate;
    private Date updateDate;
    private String name;
    private Integer age;
    private Date birth;
    private Double height;
    private Double salary;
}
