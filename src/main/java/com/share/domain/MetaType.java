package com.share.domain;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author caifan
 * @date 2019/10/20
 */
@Data
@ToString
public class MetaType {
    private Long id;
    private String type;
    private Long parentId;
    private String metaKey;
    private String metaValue;
    private String remark;
    private String level;
    private List<MetaType> metaTypeList;
}
